//
// Created by user on 10/26/20.
//

#ifndef CYGNUS_QT_BROWSEMODEL_HPP
#define CYGNUS_QT_BROWSEMODEL_HPP

#include <QObject>
#include <QAbstractListModel>
#include "messagerouter.hpp"

class BrowseModel : public QAbstractListModel {
    Q_OBJECT
public:
    enum BrowseModelRoles {
        TypeRole = Qt::UserRole + 1,
        NameRole
    };
    BrowseModel(MessageRouter *m_router, QObject *parent = nullptr);
    QHash<int, QByteArray> roleNames() const override;

    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    void init();

public slots:
    void enqueue(int idx);
    void browseTo(int index);
    void upALevel();

private slots:
    void processSourceListing(const nlohmann::json &j);
    void processBrowseListing(const nlohmann::json &j);

private:
    void browseToInternal(const QString &source, const QString &dir);

    bool m_isTopLevel;
    MessageRouter *m_messageRouter;
    QList<QString> m_sources;
    QList<QPair<QString, QString>> m_currentEntries;
    QString m_currentSource;
    QString m_currentPath;
};


#endif //CYGNUS_QT_BROWSEMODEL_HPP
