//
// Created by user on 10/24/20.
//

#include "qtrackmetadata.hpp"

QTrackMetadata::QTrackMetadata(const TrackMetadata &meta, QObject *parent) :
    m_uri(QString::fromStdString(meta.uri.value_or(""))),
    m_filename(QString::fromStdString(meta.filename.value_or(""))),
    m_title(QString::fromStdString(meta.title.value_or(""))),
    m_artist(QString::fromStdString(meta.artist.value_or(""))),
    m_album(QString::fromStdString(meta.album.value_or(""))),
    m_albumArtist(QString::fromStdString(meta.album_artist.value_or(""))),
    m_track(meta.track.value_or(0)),
    m_length(meta.length.value_or(0)),
    m_year(meta.year.value_or(0)),
    m_genre(QString::fromStdString(meta.genre.value_or(""))),
    QObject(parent)
{

}

QTrackMetadata::QTrackMetadata(QObject *parent) : QObject(parent)
{

}

void QTrackMetadata::load(const TrackMetadata &meta) {
    m_uri = QString::fromStdString(meta.uri.value_or(""));
    m_filename = QString::fromStdString(meta.filename.value_or(""));
    m_title = QString::fromStdString(meta.title.value_or(""));
    m_artist = QString::fromStdString(meta.artist.value_or(""));
    m_album = QString::fromStdString(meta.album.value_or(""));
    m_albumArtist = QString::fromStdString(meta.album_artist.value_or(""));
    m_track = meta.track.value_or(0);
    m_length = meta.length.value_or(0);
    m_year = meta.year.value_or(0);
    m_genre = QString::fromStdString(meta.genre.value_or(""));
}

