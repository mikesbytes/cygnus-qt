//
// Created by user on 10/30/20.
//

#include <rpcutils.hpp>
#include <libcygnus/rpcrequest.hpp>
#include <libcygnus/base64.hpp>
#include "coverartimageprovider.hpp"
#include <QDebug>
#include <uri.hpp>

CoverArtImageProvider::CoverArtImageProvider(cyg::SocketPtr sock) :
    QQuickImageProvider(QQuickImageProvider::Image),
    m_sock(sock)
{

}

QImage CoverArtImageProvider::requestImage(const QString &id, QSize *size, const QSize &requestedSize) {
    // unfuck Qt's URI encoding shenanigans
    std::string processed_id = cyg::percent_decode_path(id.toStdString());
    processed_id = cyg::percent_encode_path(processed_id);
    auto p_call = cyg::procedure_call_template<std::string>(
            "cover-art",
            {{"uri", "cyg:/" + processed_id}},
            "cover-art");
    qDebug() << QString::fromStdString(p_call.dump());
    cyg::RPCRequest req(p_call, m_sock);
    std::string extension;
    std::vector<uint8_t> data;
    {
        auto j = req.get_result();
        qDebug() << j["result"].size();
        if (!j.contains("result") || j.at("result").is_null()) return QImage();

        qDebug() << "got results!";
        extension = j["result"]["extension"];
        data = std::move(cyg::decode_b64(j["result"]["data"]));
    }

    QImage image;
    image.loadFromData(&data[0], data.size());
    *size = image.size();
    if (requestedSize.isValid()) {
        image = image.scaled(requestedSize, Qt::IgnoreAspectRatio, Qt::SmoothTransformation);
    }
    return image;
}
