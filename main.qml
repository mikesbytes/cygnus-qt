import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.11
import cygnus.qmlcomponents.theme 1.0
import cygnus.qmlcomponents.trackmetadata 1.0
import cygnus.qmlcomponents.browseview 1.0
import cygnus.qmlcomponents.queueview 1.0
import cygnus.qmlcomponents.playslider 1.0
import cygnus.qmlcomponents.searchsection 1.0

ApplicationWindow {
    id: window
    width: 1920
    height: 1080
    visible: true
    title: qsTr("Cygnus")


    property var queueModel: null
    property var browseModel: null
    property var artistSearchModel: null
    property var albumSearchModel: null
    property var trackSearchModel: null

    SplitView {
        anchors.fill: parent
        id: mainSplit

        handle: Rectangle {
            implicitWidth: 7
            implicitHeight: 5
            color: window.palette.window
            MouseArea {
                anchors.fill: parent
                acceptedButtons: Qt.NoButton
                cursorShape: Qt.SplitHCursor
            }
            Rectangle {
                width: 3
                height: 3
                radius: 1.5
                color: window.palette.button
                x: parent.width / 2 - 1.5
                y: parent.y + (parent.height / 2) - 10
            }
            Rectangle {
                width: 3
                height: 3
                radius: 1.5
                color: window.palette.button
                x: parent.width / 2 - 1.5
                y: parent.y + (parent.height / 2)
            }
            Rectangle {
                width: 3
                height: 3
                radius: 1.5
                color: window.palette.button
                x: parent.width / 2 - 1.5
                y: parent.y + (parent.height / 2) + 10
            }
        }

        ColumnLayout {
            SplitView.fillWidth: true
            SplitView.fillHeight: true
            SplitView.minimumWidth: 160
            SplitView.preferredWidth: mainSplit.width * .75

            spacing: 5

            PlaySlider {
                Layout.fillWidth: true
                Layout.leftMargin: 10
                Layout.rightMargin: 10
                Layout.topMargin: 10
            }

            SplitView {
                id: viewsLayoutMain
                Layout.fillWidth: true
                Layout.fillHeight: true
                Layout.topMargin: 10
                Layout.leftMargin: 5
                Layout.rightMargin: 0
                Layout.bottomMargin: 5

                handle: Rectangle {
                    implicitWidth: 7
                    implicitHeight: 5
                    color: window.palette.window
                    MouseArea {
                        anchors.fill: parent
                        acceptedButtons: Qt.NoButton
                        cursorShape: Qt.SplitHCursor
                    }
                    Rectangle {
                        width: 3
                        height: 3
                        radius: 1.5
                        color: window.palette.button
                        x: parent.width / 2 - 1.5
                        y: parent.y + (parent.height / 2) - 10
                    }
                    Rectangle {
                        width: 3
                        height: 3
                        radius: 1.5
                        color: window.palette.button
                        x: parent.width / 2 - 1.5
                        y: parent.y + (parent.height / 2)
                    }
                    Rectangle {
                        width: 3
                        height: 3
                        radius: 1.5
                        color: window.palette.button
                        x: parent.width / 2 - 1.5
                        y: parent.y + (parent.height / 2) + 10
                    }
                }
                ColumnLayout {
                    SplitView.fillHeight: true
                    SplitView.minimumWidth: 80
                    SplitView.preferredWidth: viewsLayoutMain.width * .33
                    TabBar {
                        id: libraryBar
                        Layout.fillWidth: true
                        TabButton {
                            text: qsTr("Files")
                        }
                        TabButton {
                            text: qsTr("Catalog")
                        }
                    }
                    StackLayout {
                        Layout.fillWidth: true
                        Layout.fillHeight: true
                        currentIndex: libraryBar.currentIndex
                        BrowseView {

                        } // column layout
                        SearchSection {
                        }
                    }
                }
                QueueView {
                    SplitView.fillHeight: true
                    SplitView.preferredWidth: viewsLayoutMain.width * .66
                    SplitView.minimumWidth: 80
                }
            }
        } // columnlayout

        ColumnLayout {
            SplitView.fillWidth: true
            SplitView.fillHeight: true
            SplitView.minimumWidth: 80
            SplitView.preferredWidth: mainSplit.width * .25
            spacing: 5

            RowLayout {
                spacing: 1
                Layout.alignment: Qt.AlignHCenter
                Layout.topMargin: 5
                RoundButton {
                    id: skipPrevButton
                    Layout.alignment: Qt.AlignVCenter
                    icon.name: "skip-prev"
                    radius: 3
                }
                RoundButton {
                    width: togglePauseButton.height
                    id: togglePauseButton
                    Layout.alignment: Qt.AlignVCenter
                    onClicked: {
                        cygnusApp.togglePause()
                    }
                    icon.name: "play"
                    radius: 3
                }
                RoundButton {
                    id: skipNextButton
                    width: 30
                    Layout.alignment: Qt.AlignVCenter
                    onClicked: {
                        cygnusApp.skipNext()
                    }
                    icon.name: "skip-next"
                    radius: 3
                }
            }
            ColumnLayout {
                spacing: 5
                Layout.margins: 5
                Text {
                    Layout.alignment: Qt.AlignLeft
                    id: artistText
                    text: "Track Artist"
                    color: window.palette.windowText
                }
                Text {
                    Layout.alignment: Qt.AlignLeft
                    id: albumText
                    text: "Track Album"
                    color: window.palette.windowText
                }
                Text {
                    id: titleText
                    Layout.alignment: Qt.AlignLeft
                    text: "Track Title"
                    color: Theme.colorWindowText
                }
            }
            Rectangle {
                Layout.fillWidth: true
                Layout.fillHeight: (playlistsSection.height <= 350)
                Layout.preferredHeight: width
                Layout.rightMargin: 5
                Layout.alignment: Qt.AlignTop
                color: window.palette.dark
                Image {
                    anchors.fill: parent
                    id: coverArtImg
                    asynchronous: true
                    fillMode: Image.PreserveAspectFit
                }
            }
            ColumnLayout {
                id: playlistsSection
                Layout.fillWidth: true
                Layout.fillHeight: true
                Layout.minimumHeight: 350
                Layout.rightMargin: 5
                Rectangle {
                    id: plvRect
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    color: window.palette.base
                    radius: 3
                    ListView {
                        id: playlistsView
                        anchors.fill: parent
                    }
                }
            }
        }
    } // rowlayout

    Connections {
        target: cygnusApp
        function onPlaybackTimeChanged(t) {
        }
        function onTrackChanged(t) {
            titleText.text = "Title: " + t.title
            artistText.text = "Artist: " + t.artist
            albumText.text = "Album: " + t.album
            console.log(t.uri)
            coverArtImg.source = "image://cyg-cover/" + t.uri.slice(5)
        }
    }

    Shortcut {
        sequence: "Ctrl+P"
        onActivated: cygnusApp.togglePause()
    }
}
