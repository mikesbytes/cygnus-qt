//
// Created by user on 10/24/20.
//

#ifndef CYGNUS_QT_CYGNUSQTAPP_HPP
#define CYGNUS_QT_CYGNUSQTAPP_HPP

#include <QtCore/QObject>

#include "trackmetadata.hpp"
#include "qtrackmetadata.hpp"
#include "messagerouter.hpp"

class CygnusQtApp : public QObject {
    Q_OBJECT
public:
    CygnusQtApp(QObject *parent = nullptr);
    int start(int argc, char *argv[]);

signals:
    void playbackTimeChanged(int);
    void trackChanged(QTrackMetadata*);

public slots:
    void togglePause();
    void skipTo(int index);
    void skipNext();
    void seek(int timeMs);

private slots:
    void playbackTimeChange(long time);
    void trackChange(TrackMetadata meta);

private:
    QObject *rootObj;

    TrackMetadata currentTrack;
    QTrackMetadata m_qCurrentTrack;
    MessageRouter m_messageRouter;
    long currentTime;
};


#endif //CYGNUS_QT_CYGNUSQTAPP_HPP
