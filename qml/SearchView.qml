import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.11
import cygnus.qmlcomponents.theme 1.0
import cygnus.qmlcomponents.trackmetadata 1.0

Rectangle {
    id: searchViewRect
    color: window.palette.base
    Layout.fillWidth: true
    Layout.fillHeight: true

    property alias model: searchView.model
    property bool showAlbum: false
    property bool showTrack: false
    signal setId(int id)

    ListView {
        anchors.fill: parent
        id: searchView
        model: searchViewRect.model
        clip: true
        delegate: Rectangle {
            x: 5
            width: searchView.width
            height: 20
            color: (searchView.currentIndex == index) ? window.palette.highlight : window.palette.base
            Row {
                Text {
                    text: artist + ((showAlbum) ? " - " + album : "") + ((showTrack) ? " - " + track : "")
                    color: window.palette.windowText
                }
            }
            MouseArea {
                anchors.fill: parent
                acceptedButtons: Qt.LeftButton | Qt.RightButton
                onClicked: {
                    searchView.currentIndex = index
                    if (mouse.button == Qt.RightButton)
                        searchContextMenu.popup()
                    if (mouse.button == Qt.LeftButton)
                        searchViewRect.setId(dbid)
                }
            }
        }
        Menu {
            id: searchContextMenu
            Action {
                text: "Enqueue"
                onTriggered: searchView.model.enqueue(searchView.currentIndex)
            }
        }
    }
}
