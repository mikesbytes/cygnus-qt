import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.11
import cygnus.qmlcomponents.theme 1.0
import cygnus.qmlcomponents.trackmetadata 1.0

import "qrc:/qml/stringutils.js" as StringUtils

RowLayout {
    Slider {
        id: playSlider
        objectName: "playSlider"
        height: 20;
        from: 0
        live: false
        stepSize: 1
        snapMode: Slider.SnapOnRelease
        Layout.fillWidth: true
        background: Rectangle {
            x: playSlider.leftPadding
            y: playSlider.topPadding + playSlider.availableHeight / 2 - height / 2
            implicitWidth: 200
            implicitHeight: 4
            width: playSlider.availableWidth
            height: implicitHeight
            radius: 2
            color: Theme.colorPlaySliderBackground

            Rectangle {
                width: playSlider.visualPosition * parent.width
                height: parent.height
                color: Theme.colorPlaySliderProgress
                radius: 2
            }
        }
        handle: Rectangle {
            x: playSlider.leftPadding + playSlider.visualPosition * (playSlider.availableWidth - width)
            y: playSlider.topPadding + playSlider.availableHeight / 2 - height / 2
            implicitWidth: 20;
            implicitHeight: 20;
            radius: 10;
            color: playSlider.pressed ? Theme.colorPlaySliderHandleFillActive : Theme.colorPlaySliderHandleFill
            border.color: Theme.colorPlaySliderHandleBorder
        }
        onPressedChanged: {
            if (!playSlider.pressed) {
                cygnusApp.seek(playSlider.value)
            }
        }

        Connections {
            target: cygnusApp
            function onPlaybackTimeChanged(t) {
                if (!playSlider.pressed)
                    playSlider.value = t;
            }
            function onTrackChanged(t) {
                playSlider.to = t.length
                playSlider.value = 0
            }
        }
    }

    Text {
        id: progressText
        text: "00:00/00:00"
        color: window.palette.windowText
        property int cur_length: 0

        Connections {
            target: cygnusApp
            function onPlaybackTimeChanged(t) {
                var minutes = Math.floor(t/1000/60);
                var seconds = Math.floor(t/1000) % 60;
                var t_minutes = Math.floor(progressText.cur_length/1000/60);
                var t_seconds = Math.floor(progressText.cur_length/1000 % 60)
                progressText.text = minutes.toString().padStart(2, "0") + ":" +
                                    seconds.toString().padStart(2, "0") + "/" +
                                    t_minutes.toString().padStart(2, "0") + ":" +
                                    t_seconds.toString().padStart(2, "0");

            }
            function onTrackChanged(t) {
                progressText.cur_length = t.length
            }
        }
    }
}