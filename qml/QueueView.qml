import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.11
import cygnus.qmlcomponents.theme 1.0
import cygnus.qmlcomponents.trackmetadata 1.0

Rectangle {
    Component {
        id: qvHighlight
        Rectangle {
            width: queueView.width
            height: 20
            color: Theme.colorListHighlight
            y: queueView.currentItem.y
            Behavior on y {
                SmoothedAnimation { velocity: 200; duration: 300; }
            }
        }
    }
    id: qvRect
    color: window.palette.base
    radius: 3
    HorizontalHeaderView {
        Layout.fillWidth: true
        id: queueViewHeader
        syncView: queueView
        anchors.left: queueView.left
        clip: true
        delegate: Rectangle {
            // Qt6: add cellPadding (and font etc) as public API in headerview
            readonly property real cellPadding: 8

            implicitWidth: text.implicitWidth + (cellPadding * 2)
            implicitHeight: Math.max(queueViewHeader.height, text.implicitHeight + (cellPadding * 2))
            color: window.palette.button
            border.color: window.palette.mid

            Text {
                id: text
                text: queueViewHeader.textRole ? (Array.isArray(queueViewHeader.model) ? modelData[queueViewHeader.textRole]
                                            : model[queueViewHeader.textRole])
                                       : modelData
                width: parent.width
                height: parent.height
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                color: window.palette.buttonText
            }
        }
    }
    TableView {
        anchors.top: queueViewHeader.bottom
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        id: queueView
        model: queueModel
        clip: true
        property int selectedRow: -1
        property var columnWidths: [80, 2/7, 3/7, 80, 2/7]
        property var columnsResizableWidth: parent.width - 160
        property var columnsResizable: [false, true, true, false, true]
        columnWidthProvider: function (column) {
            if (columnsResizable[column]) {
                return columnWidths[column] * columnsResizableWidth
            }
            return columnWidths[column]
        }
        delegate: Rectangle {
            color: (queueView.selectedRow == row) ? Theme.colorListHighlight :
                    (row % 2 == 0) ? window.palette.alternateBase : window.palette.base
            implicitWidth: 160
            implicitHeight: 20
            Text {
                text: (display) ? display : ""
                font.bold: row == model.currentIndex
                anchors.verticalCenter: parent.verticalCenter
                color: Theme.colorWindowText
            }
            MouseArea {
                anchors.fill: parent
                acceptedButtons: Qt.LeftButton | Qt.RightButton
                onClicked: {
                    queueView.selectedRow = row
                    console.log(queueView.selectedRow)
                    if (mouse.button == Qt.RightButton)
                        queueViewContextMenu.popup()
                }
                onDoubleClicked: {
                    cygnusApp.skipTo(row)
                }
                Menu {
                    id: queueViewContextMenu
                    Action {
                        text: "Remove"
                        onTriggered: queueModel.remove(row)
                    }
                    Action {
                        text: "Clear"
                        onTriggered: queueModel.clear()
                    }
                }
            }
        }
        ScrollBar.vertical: ScrollBar {
            policy: ScrollBar.AlwaysOn
        }
        //highlight: qvHighlight
        //highlightFollowsCurrentItem: false
        focus: true
        onWidthChanged: {
            queueView.forceLayout()
            queueView.contentHeight = 20 * queueView.rows
        }


    }
    Connections {
        target: queueModel
        function onRowsInserted() {
            queueView.forceLayout()
            queueView.contentHeight = 20 * queueView.rows
        }
        function onRowsRemoved() {
            queueView.forceLayout()
            queueView.contentHeight = 20 * queueView.rows
        }
        function onModelReset() {
            queueView.forceLayout()
            queueView.contentHeight = 20 * queueView.rows
        }
    }
}
