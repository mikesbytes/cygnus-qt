import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.11
import cygnus.qmlcomponents.theme 1.0
import cygnus.qmlcomponents.trackmetadata 1.0

ColumnLayout {
    Rectangle {
        Component {
            id: bvHighlight
            Rectangle {
                width: browseView.width
                height: 20
                color: Theme.colorListHighlight
                y: browseView.currentItem.y
                Behavior on y {
                    SmoothedAnimation { velocity: 200; duration: 300; }
                }
            }
        }

        id: browseRect
        color: window.palette.base
        radius: 3
        Layout.fillWidth: true
        Layout.fillHeight: true
        ListView {
            id: browseView
            anchors.fill: parent
            model: browseModel
            clip: true
            highlight: bvHighlight
            highlightFollowsCurrentItem: false
            focus: true
            delegate: Item {
                x: 5
                width: browseView.width
                height: 20
                Row {
                    Text {
                        text: name
                        color: window.palette.windowText
                    }
                }
                MouseArea {
                    anchors.fill: parent
                    acceptedButtons: Qt.LeftButton | Qt.RightButton
                    onClicked: {
                        browseView.currentIndex = index
                        if (mouse.button == Qt.RightButton)
                            browseViewContextMenu.popup()
                    }
                    onDoubleClicked: {
                        browseView.model.browseTo(index)
                    }

                    Menu {
                        id: browseViewContextMenu
                        Action {
                            text: "Enqueue"
                            onTriggered: browseModel.enqueue(browseView.currentIndex)
                        }
                    }
                }
            } // delegate
        } // listview
    } // rectangle
    Button {
        height: 35
        Layout.fillWidth: true
        text: "Up a level"
        onClicked: browseModel.upALevel()
    }
} // column layout
