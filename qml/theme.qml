pragma Singleton
import QtQuick 2.12

Item {
    readonly property string colorWindowBackground: "#2b231a"
    readonly property string colorWindowText: "#f4ebcd"
    readonly property string colorPanelBackground: "#46433f"
    readonly property string colorListHighlight: "#ff7600"
    readonly property string colorPlaySliderBackground: "#9a4a06"
    readonly property string colorPlaySliderProgress: "#ff7600"
    readonly property string colorPlaySliderHandleFill: "#f4ebcd"
    readonly property string colorPlaySliderHandleFillActive: "#d0c5a1"
    readonly property string colorPlaySliderHandleBorder: "#433b27"
}