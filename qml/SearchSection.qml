import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.11
import cygnus.qmlcomponents.theme 1.0
import cygnus.qmlcomponents.trackmetadata 1.0
import cygnus.qmlcomponents.searchview 1.0

Item {
    ColumnLayout {
        anchors.fill: parent
        spacing: 5
        TextField {
            id: searchStringEntry
            Layout.fillWidth: true
            placeholderText: qsTr("Search")
            selectByMouse: true
            onTextChanged: {
                artistSearchModel.setSearchStr(searchStringEntry.text)
                albumSearchModel.setSearchStr(searchStringEntry.text)
                trackSearchModel.setSearchStr(searchStringEntry.text)
            }
        }
        Text {
            text: qsTr("Artists")
            color: window.palette.windowText
        }
        SearchView {
            id: artistSearchView
            model: artistSearchModel
            onSetId: {
                albumSearchView.model.setSearchId(id)
                //trackSearchView.model.setSearchId(id)
            }
        }
        Text {
            text: qsTr("Albums")
            color: window.palette.windowText
        }
        SearchView {
            id: albumSearchView
            model: albumSearchModel
            showAlbum: true
            onSetId: {
                trackSearchView.model.setSearchId(id)
            }
        }
        Text {
            text: qsTr("Tracks")
            color: window.palette.windowText
        }
        SearchView {
            id: trackSearchView
            model: trackSearchModel
            showTrack: true
        }
    }
}
