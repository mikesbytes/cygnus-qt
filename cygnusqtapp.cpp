//
// Created by user on 10/24/20.
//

#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <rpcutils.hpp>
#include <QIcon>

#include "network.hpp"
#include "threadpool.hpp"
#include "repeattaskrunner.hpp"
#include "messagerouter.hpp"

#include "queuelistmodel.hpp"
#include "cygnusqtapp.hpp"
#include "rpcconversionfuncs.hpp"
#include "qtrackmetadata.hpp"
#include "browsemodel.hpp"
#include "coverartimageprovider.hpp"
#include "searchmodel.hpp"


Q_DECLARE_METATYPE(nlohmann::json)
Q_DECLARE_METATYPE(TrackMetadata)

int CygnusQtApp::start(int argc, char *argv[]) {
    // set up libcygnus stuff
    auto &tp = cyg::get_thread_pool("default");
    tp.spawn_workers(4);

    cyg::Network net;
    // request processing fn
    auto process_msg = [this](cyg::SocketPtr, const cyg::NetMessage &msg) {
        auto j = nlohmann::json::parse(cyg::from_net_message<std::string>(msg));
        this->m_messageRouter.process_msg(j);
    };
    net.msg_callback.connect(process_msg);
    net.new_con_callback = [](cyg::SocketPtr) {};

    cyg::RepeatTaskRunner process_in_runner([&net]() { net.process_in(); });
    cyg::RepeatTaskRunner process_out_runner([&net]() { net.process_out(); });
    process_in_runner.run(&tp);
    process_out_runner.run(&tp);

    auto client = net.connect_to("127.0.0.1", 6321);
    m_messageRouter.socket_ptr = client;
    QObject::connect(&m_messageRouter, &MessageRouter::playbackTimeChanged,
                     this, &CygnusQtApp::playbackTimeChange);
    QObject::connect(&m_messageRouter, &MessageRouter::trackChanged,
                     this, &CygnusQtApp::trackChange);

    // set up Qt stuff
    qRegisterMetaType<nlohmann::json>("json");
    qRegisterMetaType<TrackMetadata>("TrackMetadata");
    qRegisterMetaType<QList<TrackMetadata>>("TrackMetadataList");
    qmlRegisterType<QTrackMetadata>("cygnus.qmlcomponents.trackmetadata", 1, 0, "TrackMetadata");
    qmlRegisterSingletonType(QUrl("qrc:/qml/theme.qml"), "cygnus.qmlcomponents.theme", 1, 0, "Theme");
    qmlRegisterType(QUrl("qrc:/qml/BrowseView.qml"), "cygnus.qmlcomponents.browseview", 1, 0, "BrowseView");
    qmlRegisterType(QUrl("qrc:/qml/QueueView.qml"), "cygnus.qmlcomponents.queueview", 1, 0, "QueueView");
    qmlRegisterType(QUrl("qrc:/qml/PlaySlider.qml"), "cygnus.qmlcomponents.playslider", 1, 0, "PlaySlider");
    qmlRegisterType(QUrl("qrc:/qml/SearchSection.qml"), "cygnus.qmlcomponents.searchsection", 1, 0, "SearchSection");
    qmlRegisterType(QUrl("qrc:/qml/SearchView.qml"), "cygnus.qmlcomponents.searchview", 1, 0, "SearchView");

    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);

    QueueListModel queueModel(&m_messageRouter);
    BrowseModel browseModel(&m_messageRouter);
    SearchModel artistSearchModel(client, SearchModel::SearchModelMode::Artist);
    SearchModel albumSearchModel(client, SearchModel::SearchModelMode::Album);
    SearchModel trackSearchModel(client, SearchModel::SearchModelMode::Track);

    QIcon::setThemeName("cygnustheme");

    QQmlApplicationEngine engine;
    engine.rootContext()->setContextProperty(QStringLiteral("cygnusApp"), this);
    engine.addImageProvider(QStringLiteral("cyg-cover"), new CoverArtImageProvider(client));

    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
                if (!obj && url == objUrl)
                    QCoreApplication::exit(-1);
            }, Qt::QueuedConnection);
    engine.load(url);
    QObject *rootObj = engine.rootObjects()[0];//;/->findChild<QObject*>("queueView");
    if (rootObj) {
        rootObj->setProperty("queueModel", QVariant::fromValue(&queueModel));
        rootObj->setProperty("browseModel", QVariant::fromValue(&browseModel));
        rootObj->setProperty("artistSearchModel", QVariant::fromValue(&artistSearchModel));
        rootObj->setProperty("albumSearchModel", QVariant::fromValue(&albumSearchModel));
        rootObj->setProperty("trackSearchModel", QVariant::fromValue(&trackSearchModel));
    }

    m_messageRouter.init();
    queueModel.init();
    browseModel.init();

    auto res = app.exec();

    process_in_runner.stop();
    process_out_runner.stop();

    return res;
}

CygnusQtApp::CygnusQtApp(QObject *parent) :
    QObject(parent),
    rootObj(nullptr)
{

}

void CygnusQtApp::playbackTimeChange(long time) {
    currentTime = time;

    /*
    if (rootObj == nullptr) return;
    rootObj->setProperty("trackLength", (double)currentTrack.length.value_or(0));
    rootObj->setProperty("playbackTime", (double)currentTime);
     */
    emit playbackTimeChanged(time);
}

void CygnusQtApp::trackChange(TrackMetadata meta) {
    m_qCurrentTrack.load(meta);
    emit trackChanged(&m_qCurrentTrack);

    currentTrack = meta;
}

void CygnusQtApp::togglePause() {
    if (m_messageRouter.socket_ptr == nullptr) return;

    m_messageRouter.socket_ptr->queue_send(cyg::procedure_call_template("toggle-pause", {}));
}

void CygnusQtApp::skipTo(int index) {
    if (m_messageRouter.socket_ptr == nullptr) return;

    m_messageRouter.socket_ptr->queue_send(
            cyg::procedure_call_template("skip-to", {{"index", index}}));

}

void CygnusQtApp::skipNext() {
    nlohmann::json j = cyg::procedure_call_template("skip-next", {});
    m_messageRouter.socket_ptr->queue_send(j);
}

void CygnusQtApp::seek(int timeMs) {
    nlohmann::json j = cyg::procedure_call_template("seek", {{"time-ms", timeMs}});
    m_messageRouter.socket_ptr->queue_send(j);
}
