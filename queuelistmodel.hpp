#ifndef QUEUELISTMODEL_HPP
#define QUEUELISTMODEL_HPP

#include <QAbstractTableModel>
#include <trackmetadata.hpp>
#include "messagerouter.hpp"

class QueueListModel : public QAbstractTableModel
{
    Q_OBJECT
public:
    QueueListModel(MessageRouter *m_router, QObject *parent = nullptr);

    QHash<int, QByteArray> roleNames() const override;

    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;

    void init();

    Q_PROPERTY(QVariant currentIndex READ currentIndex NOTIFY currentIndexChanged)
    QVariant currentIndex();

public slots:
    void remove(int index);
    void clear();

signals:
    void currentIndexChanged();

private:
    QList<TrackMetadata> tracks_;
    MessageRouter *message_router_;
    QVariant m_currentIndex;

private slots:
    void on_process_queue_listing(const nlohmann::json &j);
    void processIndexChange(const QVariant &idx);
    void processTracksAdded(quint64 idx, QList<TrackMetadata> metaList);
    void processTracksRemoved(quint64 idx, quint64 count);
    void processTracksCleared();
};

#endif // QUEUELISTMODEL_HPP
