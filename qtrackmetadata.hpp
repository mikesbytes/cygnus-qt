//
// Created by user on 10/24/20.
//

#ifndef CYGNUS_QT_QTRACKMETADATA_HPP
#define CYGNUS_QT_QTRACKMETADATA_HPP

#include <QObject>
#include <trackmetadata.hpp>

class QTrackMetadata : public QObject {
    Q_OBJECT
public:
    QTrackMetadata(const TrackMetadata &meta, QObject *parent = nullptr);
    QTrackMetadata(QObject *parent = nullptr);

    Q_PROPERTY(QString uri MEMBER m_uri);
    Q_PROPERTY(QString filename MEMBER m_filename);
    Q_PROPERTY(QString title MEMBER m_title);
    Q_PROPERTY(QString artist MEMBER m_artist);
    Q_PROPERTY(QString album MEMBER m_album);
    Q_PROPERTY(QString albumArtist MEMBER m_albumArtist);
    Q_PROPERTY(qint32 track MEMBER m_track);
    Q_PROPERTY(qint32 length MEMBER m_length);
    Q_PROPERTY(qint32 year MEMBER m_year);
    Q_PROPERTY(QString genre MEMBER m_genre);

    void load(const TrackMetadata &meta);
private:
    QString m_uri;
    QString m_filename;
    QString m_title;
    QString m_artist;
    QString m_album;
    QString m_albumArtist;
    qint32 m_track;
    qint32 m_length;
    qint32 m_year;
    QString m_genre;
};


#endif //CYGNUS_QT_QTRACKMETADATA_HPP
