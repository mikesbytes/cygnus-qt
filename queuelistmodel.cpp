#include "queuelistmodel.hpp"

#include <bindpfn.hpp>
#include <qdebug.h>
#include <rpcutils.hpp>
#include <metadatatools.hpp>

QueueListModel::QueueListModel(MessageRouter *m_router, QObject *parent) :
    message_router_(m_router),
    QAbstractTableModel(parent)
{
    QObject::connect(m_router, &MessageRouter::queueListing,
                     this, &QueueListModel::on_process_queue_listing);
    QObject::connect(m_router, &MessageRouter::indexChanged,
                     this, &QueueListModel::processIndexChange);
    QObject::connect(m_router, &MessageRouter::tracksAdded,
                     this, &QueueListModel::processTracksAdded);
    QObject::connect(m_router, &MessageRouter::tracksRemoved,
                     this, &QueueListModel::processTracksRemoved);
    QObject::connect(m_router, &MessageRouter::tracksCleared,
                     this, &QueueListModel::processTracksCleared);
}

QHash<int, QByteArray> QueueListModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[Qt::DisplayRole] = "display";
    return roles;
}

int QueueListModel::rowCount(const QModelIndex &parent) const
{
    return tracks_.size();
}

QVariant QueueListModel::data(const QModelIndex &index, int role) const {
    if (!index.isValid()) return QVariant();

    if (role != Qt::DisplayRole || index.row() >= tracks_.size()) {
        return QVariant();
    }

    auto &track = tracks_.at(index.row());
    if (index.column() == 0) {
        if (index.row() == m_currentIndex.toInt()) {
            return "▶";
        } else {
            return QVariant();
        }
    } else if (index.column() == 1) {
        return QString::fromStdString(track.artist.value_or(""));
    } else if (index.column() == 2) {
        return QString::fromStdString(track.title.value_or(""));
    } else if (index.column() == 3) {
        if (!track.track.has_value()) return QVariant();
        return QString::number(track.track.value());
    } else if (index.column() == 4) {
        return QString::fromStdString(track.album.value_or(""));
    }
    return QVariant();
}

void QueueListModel::init() {
    auto j = cyg::procedure_call_template<std::string>("queue-metadata-listing", {}, "queue-metadata-listing");
    message_router_->socket_ptr->queue_send(j);
}

void QueueListModel::on_process_queue_listing(const nlohmann::json &j) {
    if (!j.contains("result")) return;
    if (!j["result"].contains("entries")) return;

    beginResetModel();
    if (j["result"].contains("index") && j["result"]["index"] != nullptr) {
        m_currentIndex = (quint64)j["result"]["index"].get<int64_t>();
    } else {
        m_currentIndex.clear();
    }
    tracks_.clear();
    for (auto & i : j["result"]["entries"]) {
        tracks_.push_back(i.get<TrackMetadata>());
    }
    endResetModel();
}

QVariant QueueListModel::currentIndex() {
    return m_currentIndex;
}

void QueueListModel::remove(int idx) {
    auto j = cyg::procedure_call_template("queue-remove", {{"index",idx}});
    message_router_->socket_ptr->queue_send(j);
}

void QueueListModel::processIndexChange(const QVariant &j) {
    qDebug("index change");
    qint32 old_idx = -1;
    if (!m_currentIndex.isNull()) {
        old_idx = m_currentIndex.toInt();
    }
    m_currentIndex = j;
    if (!m_currentIndex.isNull())
        emit dataChanged(index(m_currentIndex.toInt(),0), index(m_currentIndex.toInt(), 0), {Qt::DisplayRole});
    if (old_idx != -1)
        emit dataChanged(index(old_idx,0), index(old_idx, 0), {Qt::DisplayRole});
    emit currentIndexChanged();
}

void QueueListModel::processTracksAdded(quint64 idx, QList<TrackMetadata> metaList) {
    emit beginInsertRows(QModelIndex(), idx, idx + metaList.size() - 1);
    int offset = 0;
    for (auto & i : metaList) {
        tracks_.insert(idx + offset, i);
        ++offset;
    }
    emit endInsertRows();
}

void QueueListModel::processTracksRemoved(quint64 idx, quint64 count) {
    emit beginRemoveRows(QModelIndex(), idx, idx + count -1);
    tracks_.erase(tracks_.begin() + idx, tracks_.begin() + idx + count);
    emit endRemoveRows();
}

void QueueListModel::processTracksCleared() {
    emit beginResetModel();
    qDebug() << "clearing tracks";
    currentIndex().clear();
    tracks_.clear();
    emit endResetModel();
}

void QueueListModel::clear() {
    nlohmann::json j = cyg::procedure_call_template("queue-clear", {});
    message_router_->socket_ptr->queue_send(j);
}

int QueueListModel::columnCount(const QModelIndex &parent) const {
    return 5;
}

QVariant QueueListModel::headerData(int section, Qt::Orientation orientation, int role) const {
    if (section == 0) return "Playing";
    else if (section == 1) return "Artist";
    else if (section == 2) return "Title";
    else if (section == 3) return "#";
    else if (section == 4) return "Album";
    return QVariant();
}

