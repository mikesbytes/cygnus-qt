//
// Created by user on 11/1/20.
//

#include <fmt/format.h>
#include <rpcutils.hpp>
#include <libcygnus/rpcrequest.hpp>
#include "searchmodel.hpp"

SearchModel::SearchModel(cyg::SocketPtr sock, SearchModelMode mode, QObject *parent) :
    QAbstractListModel(parent),
    m_sock(sock),
    m_mode(mode)
{

}

QHash<int, QByteArray> SearchModel::roleNames() const {
    QHash<int, QByteArray> roles;
    roles[AlbumRole] = "album";
    roles[ArtistRole] = "artist";
    roles[TrackRole] = "track";
    roles[DbIdRole] = "dbid";
    return roles;
}

int SearchModel::rowCount(const QModelIndex &parent) const {
    return m_data.size();
}

QVariant SearchModel::data(const QModelIndex &index, int role) const {
    if (role == ArtistRole) {
        return std::get<0>(m_data.at(index.row()));
    } else if (role == AlbumRole) {
        return std::get<1>(m_data.at(index.row()));
    } else if (role == TrackRole) {
        return std::get<2>(m_data.at(index.row()));
    } else if (role == DbIdRole) {
        return std::get<3>(m_data.at(index.row()));
    }
    return QVariant();
}

void SearchModel::setSearchStr(const QString &query) {
    if (m_mode == SearchModelMode::Artist) {
        std::string sql =
                "SELECT artist.name AS artist_name, artist.id AS artist_id " \
            "FROM artist " \
            "WHERE artist.name LIKE ?1 " \
            "ORDER BY artist.name ASC " \
            "LIMIT 50;";

        std::string search_str = fmt::format("%{}%", query.toStdString());
        auto j = cyg::procedure_call_template<std::string>
                ("query", {{"sql",  sql},
                           {"args", {search_str}}}, "search-query-artists");
        cyg::RPCRequest req(j, m_sock);
        auto res = req.get_result();

        emit beginResetModel();
        if (!res.contains("result") ||
            !res["result"].contains("artist_name")) {
            m_data.clear();
            emit endResetModel();
            return;
        }

        m_data.clear();
        for (int i = 0; i < res["result"]["artist_name"].size(); ++i) {
            QString artist_name = QString::fromStdString(res["result"]["artist_name"][i].get<std::string>());
            int artist_id = res["result"]["artist_id"][i].get<int>();
            m_data.push_back({artist_name, "", "", artist_id});
        }
        emit endResetModel();
    } else if (m_mode == SearchModelMode::Album) {
        std::string sql =
                "SELECT " \
                "artist.name AS artist_name, " \
                "artist.id AS artist_id, " \
                "album.title AS album_title, " \
                "album.id AS album_id " \
                "FROM album " \
                "JOIN album_artist ON album.id=album_artist.album_id " \
                "JOIN artist ON artist.id=album_artist.artist_id " \
                "WHERE album.title LIKE ?1 " \
                "ORDER BY artist.name ASC " \
                "LIMIT 50;";

        std::string search_str = fmt::format("%{}%", query.toStdString());
        auto j = cyg::procedure_call_template<std::string>
                ("query", {{"sql",  sql},
                           {"args", {search_str}}}, "search-query-albums");
        cyg::RPCRequest req(j, m_sock);
        auto res = req.get_result();

        emit beginResetModel();
        if (!res.contains("result") ||
            !res["result"].contains("album_title")) {
            m_data.clear();
            emit endResetModel();
            return;
        }

        m_data.clear();
        for (int i = 0; i < res["result"]["album_title"].size(); ++i) {
            QString artist_name = QString::fromStdString(res["result"]["artist_name"][i].get<std::string>());
            QString album_title = QString::fromStdString(res["result"]["album_title"][i].get<std::string>());
            int album_id = res["result"]["album_id"][i].get<int>();
            m_data.push_back({artist_name, album_title, "", album_id});
        }
        emit endResetModel();
    } else if (m_mode == SearchModelMode::Track) {
        std::string sql =
                "SELECT " \
                "track.title AS track_title, " \
                "track.id AS track_id, " \
                "artist.name AS artist_name, " \
                "artist.id AS artist_id, " \
                "album.title AS album_title, " \
                "album.id AS album_id " \
                "FROM track " \
                "JOIN album ON album.id=track.album_id " \
                "JOIN album_artist ON album.id=album_artist.album_id " \
                "JOIN artist ON artist.id=album_artist.artist_id " \
                "WHERE track.title LIKE ?1 " \
                "ORDER BY artist.name, track.title ASC " \
                "LIMIT 50;";

        std::string search_str = fmt::format("%{}%", query.toStdString());
        auto j = cyg::procedure_call_template<std::string>
                ("query", {{"sql",  sql},
                           {"args", {search_str}}}, "search-query-tracks");
        cyg::RPCRequest req(j, m_sock);
        auto res = req.get_result();

        emit beginResetModel();
        if (!res.contains("result") ||
            !res["result"].contains("track_title")) {
            m_data.clear();
            emit endResetModel();
            return;
        }

        m_data.clear();
        for (int i = 0; i < res["result"]["track_title"].size(); ++i) {
            QString artist_name = QString::fromStdString(res["result"]["artist_name"][i].get<std::string>());
            QString album_title = QString::fromStdString(res["result"]["album_title"][i].get<std::string>());
            QString track_title = QString::fromStdString(res["result"]["track_title"][i].get<std::string>());
            int track_id = res["result"]["track_id"][i].get<int>();
            m_data.push_back({artist_name, album_title, track_title, track_id});
        }
        emit endResetModel();
    }
}

void SearchModel::setSearchId(int id) {
    if (m_mode == SearchModelMode::Artist) {
        return;
    } else if (m_mode == SearchModelMode::Album) {
        std::string sql =
                "SELECT " \
                "artist.name AS artist_name, " \
                "artist.id AS artist_id, " \
                "album.title AS album_title, " \
                "album.id AS album_id " \
                "FROM album " \
                "JOIN album_artist ON album.id=album_artist.album_id " \
                "JOIN artist ON artist.id=album_artist.artist_id " \
                "WHERE album_artist.artist_id=?1 " \
                "ORDER BY artist.name ASC " \
                "LIMIT 50;";

        auto j = cyg::procedure_call_template<std::string>
                ("query", {{"sql",  sql},
                           {"args", {id}}}, "search-query-albums");
        cyg::RPCRequest req(j, m_sock);
        auto res = req.get_result();

        emit beginResetModel();
        if (!res.contains("result") ||
            !res["result"].contains("album_title")) {
            m_data.clear();
            emit endResetModel();
            return;
        }

        m_data.clear();
        for (int i = 0; i < res["result"]["album_title"].size(); ++i) {
            QString artist_name = QString::fromStdString(res["result"]["artist_name"][i].get<std::string>());
            QString album_title = QString::fromStdString(res["result"]["album_title"][i].get<std::string>());
            int album_id = res["result"]["album_id"][i].get<int>();
            m_data.push_back({artist_name, album_title, "", album_id});
        }
        emit endResetModel();
    } else if (m_mode == SearchModelMode::Track) {
        std::string sql =
                "SELECT " \
                "track.title AS track_title, " \
                "track.id AS track_id, " \
                "artist.name AS artist_name, " \
                "artist.id AS artist_id, " \
                "album.title AS album_title, " \
                "album.id AS album_id " \
                "FROM track " \
                "JOIN album ON album.id=track.album_id " \
                "JOIN album_artist ON album.id=album_artist.album_id " \
                "JOIN artist ON artist.id=album_artist.artist_id " \
                "WHERE track.album_id=?1 " \
                "ORDER BY artist.name, track.title ASC " \
                "LIMIT 50;";

        auto j = cyg::procedure_call_template<std::string>
                ("query", {{"sql",  sql},
                           {"args", {id}}}, "search-query-tracks");
        cyg::RPCRequest req(j, m_sock);
        auto res = req.get_result();

        emit beginResetModel();
        if (!res.contains("result") ||
            !res["result"].contains("track_title")) {
            m_data.clear();
            emit endResetModel();
            return;
        }

        m_data.clear();
        for (int i = 0; i < res["result"]["track_title"].size(); ++i) {
            QString artist_name = QString::fromStdString(res["result"]["artist_name"][i].get<std::string>());
            QString album_title = QString::fromStdString(res["result"]["album_title"][i].get<std::string>());
            QString track_title = QString::fromStdString(res["result"]["track_title"][i].get<std::string>());
            int track_id = res["result"]["track_id"][i].get<int>();
            m_data.push_back({artist_name, album_title, track_title, track_id});
        }
        emit endResetModel();
    }

}

void SearchModel::enqueue(int idx) {
    if (m_mode == SearchModelMode::Artist) {
        int artistId = std::get<3>(m_data.at(idx));
        auto j = cyg::procedure_call_template(
                "queue-add", {{"artist_id", artistId}}
        );
        m_sock->queue_send(j);
    } else if (m_mode == SearchModelMode::Album) {
        int albumId = std::get<3>(m_data.at(idx));
        auto j = cyg::procedure_call_template(
                "queue-add", {{"album_id", albumId}}
        );
        m_sock->queue_send(j);
    } else if (m_mode == SearchModelMode::Track) {
        int trackId = std::get<3>(m_data.at(idx));
        auto j = cyg::procedure_call_template(
                "queue-add", {{"track_id", trackId}}
        );
        m_sock->queue_send(j);
    }

}
