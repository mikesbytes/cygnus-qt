//
// Created by user on 10/23/20.
//

#ifndef CYGNUS_QT_MESSAGEROUTER_HPP
#define CYGNUS_QT_MESSAGEROUTER_HPP

#include <network.hpp>
#include <nlohmann/json.hpp>
#include <map>
#include <QtCore/QObject>
#include <trackmetadata.hpp>
#include <QVariant>

class MessageRouter : public QObject {
    Q_OBJECT
public:
    MessageRouter(QObject *parent = nullptr);

    void process_msg(const nlohmann::json &j);
    void init();

    cyg::SocketPtr socket_ptr;

signals:
    void queueListing(const nlohmann::json &j);
    void playbackTimeChanged(int);
    void trackChanged(TrackMetadata);
    void indexChanged(QVariant);
    void tracksAdded(quint64 index, QList<TrackMetadata>);
    void tracksRemoved(quint64 index, quint64 count);
    void tracksCleared();
    void listSources(const nlohmann::json &j);
    void browseTo(const nlohmann::json &j);
};


#endif //CYGNUS_QT_MESSAGEROUTER_HPP
