//
// Created by user on 10/26/20.
//

#include <rpcutils.hpp>
#include <filesystem>
#include <qdebug.h>
#include "browsemodel.hpp"
#include <libcygnus/uriutils.hpp>

BrowseModel::BrowseModel(MessageRouter *m_router, QObject *parent) :
    QAbstractListModel(parent),
    m_messageRouter(m_router),
    m_isTopLevel(true)
{
    QObject::connect(m_router, &MessageRouter::listSources,
                     this, &BrowseModel::processSourceListing);
    QObject::connect(m_router, &MessageRouter::browseTo,
                     this, &BrowseModel::processBrowseListing);
}

QHash<int, QByteArray> BrowseModel::roleNames() const {
    QHash<int, QByteArray> roles;
    roles[TypeRole] = "type";
    roles[NameRole] = "name";
    return roles;
}

int BrowseModel::rowCount(const QModelIndex &parent) const {
    if (m_isTopLevel) return m_sources.size();
    else return m_currentEntries.size();
}

QVariant BrowseModel::data(const QModelIndex &index, int role) const {
    if (!index.isValid()) return QVariant();

    if (role == Qt::DisplayRole) {
        return "N/A";
    }
    if (role == TypeRole) {
        if (m_isTopLevel) {
            return "source";
        } else {
            return m_currentEntries.at(index.row()).first;
        }
    }
    if (role == NameRole) {
        if (m_isTopLevel) {
            return m_sources.at(index.row());
        } else {
            return m_currentEntries.at(index.row()).second;
        }
    }

    return QVariant();
}

void BrowseModel::init() {
    auto j = cyg::procedure_call_template<std::string>("library-list-sources", {}, "list-sources");
    m_messageRouter->socket_ptr->queue_send(j);
}

void BrowseModel::browseTo(int index) {
    if (m_isTopLevel) {
        browseToInternal(m_sources.at(index), "/");
    } else if (m_currentEntries.at(index).first == "dir"){
        browseToInternal(m_currentSource, m_currentEntries.at(index).second);
    }
}

void BrowseModel::processSourceListing(const nlohmann::json &j) {
    if (!j.contains("result")) return;
    if (!j["result"].is_array()) return;

    if (m_isTopLevel) emit beginResetModel();
    m_sources.clear();
    for (auto & i : j["result"]) {
        m_sources.push_back(QString::fromStdString(i));
    }
    if (m_isTopLevel) emit endResetModel();
}

void BrowseModel::processBrowseListing(const nlohmann::json &j) {
    qDebug() << "browse listing";
    if (!j.contains("result")) return;
    if (!j["result"].contains("entries") ||
        !j["result"].contains("path") ||
        !j["result"].contains("source")) return;

    emit beginResetModel();
    m_isTopLevel = false;
    m_currentSource = QString::fromStdString(j["result"]["source"]);
    m_currentPath = QString::fromStdString(j["result"]["path"]);
    m_currentEntries.clear();
    for (auto & i : j["result"]["entries"]) {
        m_currentEntries.push_back(QPair(QString::fromStdString(i["type"]),
                                         QString::fromStdString(i["name"])));
    }
    emit endResetModel();
}

void BrowseModel::upALevel() {
    if (m_isTopLevel) return;
    if (m_currentPath == "/") {
        emit beginResetModel();
        m_isTopLevel = true;
        emit endResetModel();
    } else {
        std::string newPath = std::filesystem::path(m_currentPath.toStdString()).parent_path();
        browseToInternal(m_currentSource, QString::fromStdString(newPath));
    }
}

void BrowseModel::browseToInternal(const QString &source, const QString &dir) {
    auto j = cyg::procedure_call_template<std::string>("list-dir", {{"source",source.toStdString()},{"path",dir.toStdString()}}, "browse-to");
    m_messageRouter->socket_ptr->queue_send(j);
}

void BrowseModel::enqueue(int idx) {
    if (m_isTopLevel) return;
    std::string trackName = m_currentEntries.at(idx).second.toStdString();
    nlohmann::json j = cyg::procedure_call_template(
            "queue-add", {{"uri", cyg::uri_from_source_path(m_currentSource.toStdString(), trackName)}});
    m_messageRouter->socket_ptr->queue_send(j);
}
