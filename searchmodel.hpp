//
// Created by user on 11/1/20.
//

#ifndef CYGNUS_QT_SEARCHMODEL_HPP
#define CYGNUS_QT_SEARCHMODEL_HPP


#include <QtCore/QAbstractListModel>
#include <network.hpp>

class SearchModel : public QAbstractListModel {
    Q_OBJECT
public:
    enum SearchModelRoles {
        ArtistRole = Qt::UserRole + 1,
        AlbumRole,
        TrackRole,
        DbIdRole
    };

    enum class SearchModelMode {
        Album,
        Artist,
        Track
    };
    SearchModel(cyg::SocketPtr sock, SearchModelMode mode, QObject *parent = nullptr);
    QHash<int, QByteArray> roleNames() const override;

    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;


public slots:
    void setSearchStr(const QString &query);
    void setSearchId(int id);
    void enqueue(int idx);

private:
    cyg::SocketPtr m_sock;
    SearchModelMode m_mode;

    std::vector<std::tuple<QString, QString, QString, int>> m_data;
};


#endif //CYGNUS_QT_SEARCHMODEL_HPP
