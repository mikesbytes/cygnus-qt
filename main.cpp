#include "cygnusqtapp.hpp"

int main(int argc, char *argv[])
{
    CygnusQtApp cygApp;
    return cygApp.start(argc, argv);
}
