//
// Created by user on 10/30/20.
//

#ifndef CYGNUS_QT_COVERARTIMAGEPROVIDER_HPP
#define CYGNUS_QT_COVERARTIMAGEPROVIDER_HPP

#include <QtQuick/QQuickImageProvider>
#include <network.hpp>

class CoverArtImageProvider : public QQuickImageProvider {
public:
    CoverArtImageProvider(cyg::SocketPtr sock);

    QImage requestImage(const QString &id, QSize *size, const QSize &requestedSize) override;
private:
    cyg::SocketPtr m_sock;
};


#endif //CYGNUS_QT_COVERARTIMAGEPROVIDER_HPP
