//
// Created by user on 10/23/20.
//

#include <rpcutils.hpp>
#include "messagerouter.hpp"
#include "metadatatools.hpp"
#include <QVariant>

MessageRouter::MessageRouter(QObject *parent) :
    QObject(parent),
    socket_ptr(nullptr)
{
}

void MessageRouter::process_msg(const nlohmann::json &j) {
    if (j.contains("id")) {
        if (j["id"] == "queue-metadata-listing") {
            emit queueListing(j);
        } else if (j["id"] == "playhead-info") {
            emit trackChanged(j["result"].get<TrackMetadata>());
        } else if (j["id"] == "list-sources") {
            emit listSources(j);
        } else if (j["id"] == "browse-to") {
            emit browseTo(j);
        }
    } else if (j.contains("method")) {
        if (j["method"] == "playback-time") {
            emit playbackTimeChanged(j["params"]["time"]);
        } else if (j["method"] == "playhead-changed") {
            emit trackChanged(j["params"].get<TrackMetadata>());
            if (j["params"].contains("index") && j["params"]["index"] != nullptr) {
                emit indexChanged((quint64)j["params"]["index"].get<uint64_t>());
            } else {
                emit indexChanged(QVariant());
            }
        } else if (j["method"] == "playqueue") {
            if (j["params"]["mode"] == "add") {
                quint64 idx = j["params"]["index"].get<size_t>();
                QList<TrackMetadata> metaList;
                for (auto &i : j["params"]["entries"]) {
                    metaList.push_back(i.get<TrackMetadata>());
                }
                emit tracksAdded(idx, metaList);
            } else if (j["params"]["mode"] == "remove") {
                emit tracksRemoved(j["params"]["index"], j["params"]["count"]);
            } else if (j["params"]["mode"] == "clear") {
                emit tracksCleared();
            }
        }
    }
}

void MessageRouter::init() {
    socket_ptr->queue_send(
            cyg::procedure_call_template("subscribe", {{"subscribe-to", "playback-time"}})
    );
    socket_ptr->queue_send(
            cyg::procedure_call_template("subscribe", {{"subscribe-to", "playhead-changed"}})
    );
    socket_ptr->queue_send(cyg::procedure_call_template("subscribe", {{"subscribe-to", "playqueue"}}));
    socket_ptr->queue_send(
            cyg::procedure_call_template<std::string>("playhead-info", {}, "playhead-info")
    );
}

